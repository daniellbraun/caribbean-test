<?php

/*

Template Name: Specials

*/

?><?php global $more;
$theme_options = get_option('caribbeanclubresort');
get_header(); ?>



<div id="maincontent">
  <div id="orgBar"></div>
  <div id="mcLeft">
    
    <div id="mainTitles">
      <h1>Specials</h1></div>
    <span class="fs1"><?php go_get_specials(); ?></span>
 
  </div>
  <div id="mcRight">
    <?php 
	  get_sidebar();
	 
	 fetch_specials();
	  ?>
  </div>
  <div id="ftClear"></div>
</div>
</div>
<?php get_footer(); ?>
