<?php global $more;
$theme_options = get_option('caribbeanclubresort');
get_header(); ?>

<div id="maincontent">
  <div id="orgBar"></div>
  <div id="mcLeft">
    <h1>Welcome to Caribbean Club Wisconsin Dells</h1>
    <br />
    <span class="f1"> BEST DEAL AROUND" Escape to paradise in this Wisconsin Dells Resort. The Caribbean 
    Club Resort, located on beautiful Lake Delton, will surround you with lush gardens, 
    palms, cascading waterfalls, and romantic sunsets. <br />
    <br />
    At the Caribbean Club Resort, you will be convinced you are far away from all 
    earthly concerns, but are only minutes away from everything the Wisconsin Dells 
    has to offer - major attractions, casino, golf, public indoor & outdoor water parks,
    and top notch restaurants. <br />
    <br />
    We can't wait to see you and your family at Caribbean Club of Wisconsin Dells!
    For Reservations & Information call today for best rates! </span>
    
    <div id="passWrap"><div class="pass"><a href="http://www.passporttosavings.com/Motel_Web_Sales_Tool_2017.pdf" target="_blank"><img src="/wp-content/themes/caribbeanclub/images/passportLogo.jpg" border="0" /></a>Stay with us and SAVE 25% on one or more of 13 Wisconsin Dells attractions! <a href="http://www.passporttosavings.com/Motel_Web_Sales_Tool_2017.pdf" target="_blank">CLICK HERE</a> for details.</div></div>
    
    <div id="affilWrap">
     <div class="affiliate"><a href="http://www.umbrellavacations.com/" target="_blank"><img src="/wp-content/themes/caribbeanclub/images/umbLogo.jpg" border="0" /></a></div>
      <div class="affiliate"> <a href="http://www.tripadvisor.com/Hotel_Review-g60403-d262662-Reviews-Caribbean_Club_Resort-Wisconsin_Dells_Wisconsin.html/"><img src="/wp-content/themes/caribbeanclub/images/trpadvLogo.jpg" border="0" /></a></div>
      <div class="affiliate"><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FUmbrella-Vacations%2F232045430160653&amp;send=false&amp;layout=button_count&amp;width=131&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; margin:20px 0px 0px 9px; overflow:hidden; width:131px; height:21px;" allowTransparency="true"></iframe></div>
    </div>
  </div>
  <div id="mcRight">
    <?php 
	  get_sidebar();
	 
	 fetch_specials();
	  ?>
      
  </div>
  <div id="rmSliderwrap">
    <?php displayScroller(); ?>
    
    <!--MONKS SCROLLER-->
    
    <?php 
	/*
	<div id="carouselwrapper" style="margin-left:1px;">
      <div class="theme-default">
        <div class="arrow-right">
          <p class="arrow"> <a href="javascript:stepcarousel.stepBy('carousel_0', 1)"> <span class="hide">Forward 1 panel</span> </a> </p>
        </div>
        <div class="arrow-left">
          <p class="arrow"> <a href="javascript:stepcarousel.stepBy('carousel_0', -1)"> <span class="hide">Back 1 panel</span> </a> </p>
        </div>
        <div id="carousel_0" class="stepcarousel">
          <div class="belt">
            <div class="panel" style="width:300;"><a href="http://www.monksbarandgrill.com/cheeseburgers" > <img width="212" height="269" src="http://www.monksbarandgrill.com/wp-content/uploads/2011/10/carousel-template-cheeseburger1.png" class="attachment-post-thumbnail wp-post-image" alt="carousel-template-cheeseburger" title="carousel-template-cheeseburger" /> </a>
              <div class="panel-text"> </div>
            </div>
            <div class="panel" style="width:300;"><a href="http://www.monksbarandgrill.com/cheesecurds.html" > <img width="212" height="269" src="http://www.monksbarandgrill.com/wp-content/uploads/2011/10/carousel-template-cheesecurds1.png" class="attachment-post-thumbnail wp-post-image" alt="carousel-template-cheesecurds" title="carousel-template-cheesecurds" /> </a>
              <div class="panel-text"> </div>
            </div>
            <div class="panel" style="width:300;"><a href="http://www.monksbarandgrill.com/monster-burger.html" > <img width="212" height="269" src="http://www.monksbarandgrill.com/wp-content/uploads/2011/10/carousel-template-monsterburger1.png" class="attachment-post-thumbnail wp-post-image" alt="carousel-template-monsterburger" title="carousel-template-monsterburger" /> </a>
              <div class="panel-text"> </div>
            </div>
            <div class="panel" style="width:300;"><a href="http://www.monksbarandgrill.com/black-blue-burger.html" > <img width="212" height="269" src="http://www.monksbarandgrill.com/wp-content/uploads/2011/10/carousel-template-blackbleuburger2.png" class="attachment-post-thumbnail wp-post-image" alt="carousel-template-black&amp;bleuburger" title="carousel-template-black&amp;bleuburger" /> </a>
              <div class="panel-text"> </div>
            </div>
            <div class="panel" style="width:300;"><a href="http://www.monksbarandgrill.com/the-wing-thing.html" > <img width="212" height="269" src="http://www.monksbarandgrill.com/wp-content/uploads/2011/10/carousel-template1.png" class="attachment-post-thumbnail wp-post-image" alt="carousel-template" title="carousel-template" /> </a>
              <div class="panel-text"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	*/
	?>
  </div>
  <div id="content-lowercap"></div>
</div>
<script type='text/javascript' src='/wp-content/themes/caribbeanclub/stepcarousel.js?ver=3.1.4'></script> 
<script type="text/javascript">
			stepcarousel.setup({
				galleryid: 'carousel_0', //id of carousel DIV
				beltclass: 'belt', //class of inner "belt" DIV containing all the panel DIVs
				panelclass: 'panel', //class of panel DIVs each holding content
				autostep: {enable:true, moveby:1, pause:5000},
				panelbehavior: {speed:500, wraparound:true, persist:true},
				defaultbuttons: {enable: false, moveby: 1, leftnav: ['http://i34.tinypic.com/317e0s5.gif', -5, 80], rightnav: ['http://i38.tinypic.com/33o7di8.gif', -20, 80]},
				statusvars: ['statusA', 'statusB', 'statusC'], //register 3 variables that contain current panel (start), current panel (last), and total panels
				contenttype: ['inline'] //content setting ['inline'] or ['ajax', 'path_to_external_file']
			})
			</script>
</div>
</div>
</div>
<script type="text/javascript">
var pager = new Imtech.Pager();
$(document).ready(function() {
    pager.paragraphsPerPage = 2; // set amount elements per page
    pager.pagingContainer = $('#content'); // set of main container
    pager.paragraphs = $('div.z', pager.pagingContainer); // set of required containers
    pager.showPage(1);
});
</script> 

<?php get_footer(); ?>
