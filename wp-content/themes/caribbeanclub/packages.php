<?php

/*

Template Name: Packages

*/

?><?php global $more;
$theme_options = get_option('caribbeanclubresort');
get_header(); ?>



<div id="maincontent">
  <div id="orgBar"></div>
  <div id="mcLeft">
   <?php
   $m = date("j");
   
   
   ?><?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div id="mainTitles"><h1><?php echo get_the_title(); ?></h1></div>
    <span class="fs1"><?php 
	//the_content(); 
	
	
	if (date("n") >=5 && date("n") <= 8) {
		require("summerpacakges.html");
	} else {
		require("off-season-packages.html");
	}
	
	?></span>
    <?php endwhile; ?>
  </div>
  <div id="mcRight">
    <?php 
	  get_sidebar();
	 
	 fetch_specials();
	  ?>
  </div>
  <div id="ftClear"></div>
</div>
</div>
<?php get_footer(); ?>
