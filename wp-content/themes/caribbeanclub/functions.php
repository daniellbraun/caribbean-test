<?php


register_sidebar(array(
'name'=>'sidebar-right-default',
'description' => 'Right Sidebar Default.',
'before_widget' => '',
'after_widget' => '',
'before_title' => '',
'after_title' => '',
));





//load frontend scripts
function script_loader() {
	if (!is_admin()) {
		wp_register_style('style', get_bloginfo('stylesheet_url'));
		wp_enqueue_style('style');
		
		wp_deregister_script('jquery' );
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js');
		wp_enqueue_script('jquery');
		
		wp_register_script('nivo', get_bloginfo('template_directory').'/js/jquery.nivo.slider.js');
		wp_enqueue_script('nivo');
		wp_register_style('nivostyle', get_bloginfo('template_directory').'/nivo-slider.css');
		wp_enqueue_style('nivostyle');

		wp_register_script('cufon', get_bloginfo('template_directory').'/fonts/cufon-yui.js');
		wp_enqueue_script('cufon');
		wp_register_script('font', get_bloginfo('template_directory').'/fonts/Junction_400.font.js');
		wp_enqueue_script('font');
		wp_register_script('font2', get_bloginfo('template_directory').'/fonts/OFL_Sorts_Mill_Goudy_TT_italic_500.font.js');
		wp_enqueue_script('font2');
	}
}




function displayScroller ()
{
	?><div id="featuredBackground"><div id="featuredTab"><img src="/wp-content/themes/caribbeanclub/images/featuredTab.png" border="0" /></div> <div id="tileWrap">  <div id="carouselwrapper" style="margin-left:1px;"><div class="theme-default"><div class="arrow-right">
        <p class="arrow"> <a href="javascript:stepcarousel.stepBy('carousel_0', 1)"> <span class="hide">Forward 1 panel</span> </a> </p>
      </div>
      <div class="arrow-left">
        <p class="arrow"> <a href="javascript:stepcarousel.stepBy('carousel_0', -1)"> <span class="hide">Back 1 panel</span> </a> </p>
      </div><div id="carousel_0" class="stepcarousel"><div class="belt">  
 


       <?php
	
	$qpargs = array(
		'post_type' => 'Featured',
		'posts_per_page' => -1

	);	

	query_posts($qpargs);
	
	 $i = 0;
	
	if(have_posts()) {
		 
		 while (have_posts() ) : the_post(); 
		
			$i = $i + 1 ;
			
			
			$thepostid=get_the_ID();
			
			//Justin...start editing here for each room special
  echo '<div class="panel" >';             
if ($i % 2 > 0 ) echo '<div class="featuredTile1">  ';
else echo '<div class="featuredTile2"> ';
?>

  <div id="roomTitle"> <?php echo get_the_title($thepostid);?></div>
  <div id="roomImage"><?php echo get_the_post_thumbnail($thepostid);?></div>
  <div id="featuredDescription"> <?php echo get_the_content()?></div>
</div></div>
<?php
			//Justin...end of room special editing

		endwhile; 
	
	} 
	
		
	?></div></div></div></div><?php
}




add_action( 'init', 'create_featured' );
function create_featured() {
  $labels = array(
    'name' => _x('Featured', 'post type general name'),
    'singular_name' => _x('Featured', 'post type singular name'),
    'add_new' => _x('Add New', 'Featured'),
    'add_new_item' => __('Add New Featured'),
    'edit_item' => __('Edit Featured'),
    'new_item' => __('New Featured'),
    'view_item' => __('View Featured'),
    'search_items' => __('Search Featured'),
    'not_found' =>  __('No Featured found'),
    'not_found_in_trash' => __('No Featured found in Trash'),
    'parent_item_colon' => ''
  );

  $supports = array('title', 'editor', 'custom-fields', 'revisions', 'excerpt','thumbnail');

  register_post_type( 'featured',
    array(
      'labels' => $labels,
      'public' => true,
      'supports' => $supports,
	  'taxonomies' => array('category'),
    )
  );  
}




function fetch_specials() {
	?>
    <div class='specialscontainer' >
    <?php
	

	
	$qpargs = array(
		'post_type' => 'Events',
		'posts_per_page' => -1

	);	

	query_posts($qpargs);
	
	$i = 0;
 	if(have_posts()) {
		 
		 while (have_posts() ) : the_post(); 
			
			$thepostid=get_the_ID();
			
			if ($i % 2 < 1) {
				echo '<div class="eventpage" id="eventpage'.$i.'">';
			}
						
			$i = $i + 1 ;
			//echo $thepostid;

			echo '    <table class="eventWrapTop" border="0" cellpadding="0" cellspacing="0" width="287">
      <tbody><tr>
        <td class="imageWrap" valign="top" width="116">'.get_the_post_thumbnail($thepostid,'specialsthumb').'</td>
        <td class="eventDescription" valign="top" width="171"><span class="eventTitle">'.get_the_title($thepostid).'</span><br>'.get_the_excerpt($thepostid).'</td>
      </tr>
    </tbody></table>';
	
			if ($i % 2 < 1 && $i > 1) {
				echo '</div>';
			}
			
				
		endwhile; 
		
		if ($i % 2 == 1) {
						echo '    <table class="eventWrapTop" border="0" cellpadding="0" cellspacing="0" width="287">
      <tbody><tr>
        <td class="imageWrap" valign="top" width="116">&nbsp;</td>
        <td class="eventDescription" valign="top" width="171"><span class="eventTitle">&nbsp;</span><br>&nbsp;</td>
      </tr>
    </tbody></table>';
			echo '</div>';
		}
	} 
	?>
    <div id="pagingControls2">Page: <?php
    	$spc = 0;
		for ($ii = 0;$ii < $i;$ii = $ii + 2) {
			$spc++;
			echo '<a style="cursor:pointer" onclick="$(\'.eventpage\').hide();$(\'#eventpage'.$ii.'\').show();return false;">'.$spc.'</a>&nbsp;';
		}
	?>
</div>
    </div>
    <?php	
	//<img class="eventImage" src="'.get_the_post_thumbnail($thepostid).'">
}


add_image_size('specialsthumb',96,64,true);



//custom media image sizes
/*if ( function_exists( 'add_image_size' ) ) {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'menucard_thumb', 9999, 48 );
	add_image_size( 'gallery_thumb', 260, 9999 );
	add_image_size( 'mediumwidth', 586, 9999 );
	add_image_size( 'fullwidth', 906, 9999 );
}

*/


function go_get_testimonials() {
//	echo "it worked";
//<div id="testimonialWrap">
//<div id="testimonialWrap2">

	$qpargs = array(
		'post_type' => 'Testimonial',
		'posts_per_page' => -1

	);	

	query_posts($qpargs);
	
	if(have_posts()) {
		while (have_posts() ) : the_post(); 

		$thepostid=get_the_ID();
		
		$colorwrap = get_post_meta($thepostid, 'field_select__1', true);
		
		
		if ($colorwrap=="orange") {
			echo '<div id="testimonialWrap1">'.get_the_excerpt($thepostid).'</div>';
		} else {
			echo '<div id="testimonialWrap2">'.get_the_excerpt($thepostid).'</div>';
		}
		
		
		endwhile;	
	}
	
 	/*
	if(have_posts()) {
		 
		
		while (have_posts() ) : the_post(); 

			$thepostid=get_the_ID();
			//echo $thepostid;

			echo '<div id="testimonialWrap">'.$thepostid.'</div>';
			
				
		endwhile; 
		
		
		}
		*/
}

function go_get_specials() {


	/*if ($month < 5 || $month > 8) {
		$season = "Main";	
	} else {
		$season = "Off";	
	}*/

	/*

	$qpargs = array(
		'post_type' => 'Specials',
		'posts_per_page' => -1,
		
		'meta_query' => array(
			array(
				'key' => "field_select__2",
				'value' => $season,
				'compare' => "=="
			)
		)
	);	*/

	$qpargs = array(
		'post_type' => 'Specials',
		'posts_per_page' => -1
	);

	query_posts($qpargs);
	
	if(have_posts()) {
		while (have_posts() ) : the_post(); 

		$thepostid=get_the_ID();
		
		echo "<table border='0' cellpadding='0' cellspacing='0' class='specialtable'><tr><td valign='top' class='specialtable_cell1'>".get_the_post_thumbnail($thepostid)."</td><td valign='top'  class='specialtable_cell2'><div class='specialtitle'>".get_the_title($thepostid)."</div><div class='specialcontent'>".get_the_content()."</div></td></tr></table>";		
		echo "<p>&nbsp;</p><div class='pageDash'>&nbsp;</div><p>&nbsp;</p>";

		
		
		endwhile;	
	}
	
	

}


add_filter('comment_post_redirect', 'redirect_after_comment');
function redirect_after_comment($location)
{
//return $_SERVER["HTTP_REFERER"];
return "/comments.html";
}




//create the breadcrumb

function the_breadcrumb() {
	if (!is_home()) {
		echo '<div id="breadcrumbs">';
		echo '<a href="'.get_bloginfo("url").'">Home</a>';
		if (is_category() || is_single()) {
			$cat_ID = get_query_var('cat');
			if ($cat_ID) {
				echo ' / '.get_cat_name($cat_ID);
			} else {
				$categories = array_reverse(get_the_category());
				foreach($categories as $category) {
					echo ' / <a href="'.get_category_link($category->term_id).'">'.__($category->cat_name).'</a>';
				} 	
			}
			if (is_single()) {
				echo ' / ';
				the_title();
			}
		} elseif (is_page()) {
			echo ' / ';
			echo the_title();
		}
		echo '</div>';
	}
}




?>
