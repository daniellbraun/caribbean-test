<?php

/*

Template Name: Gallery Template

*/

?><?php global $more;
$theme_options = get_option('caribbeanclubresort');
get_header('gallery'); ?>

<div id="maincontentGal">
  <div id="mcLeftGal">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div id="mainTitles"><h1><?php echo get_the_title(); ?></h1></div>
    <span class="fs1">

	<?php //echo do_shortcode('[nivo gallery=2]'); ?>

	
	<?php the_content(); ?></span>
    <?php endwhile; ?>
  </div>
  <div id="mcRightGal">
    <?php 
	  get_sidebar();
	 
	 fetch_specials();
	  ?>
  </div>
  <div id="ftClear"></div>
</div>
</div>
<?php get_footer(); ?>

