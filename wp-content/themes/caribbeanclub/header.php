<?php $theme_options = get_option('caribbeanclubresort'); ?><!DOCTYPE html>
<html>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="description" content="<?php bloginfo('description') ?>" />
<meta name="generator" content="WordPress <?php bloginfo('version') ?>" />
<!-- Please leave for stats -->
<title>
<?php bloginfo('name'); ?>
<?php if ( is_single() ) { ?>
Blog Archive
<?php } ?>
<?php wp_title(); ?>
</title>
<?php wp_head() ?>
<link rel="icon" href="<?php echo $theme_options['cp_favicon']; ?>" type="image/x-icon" />
<link rel="shortcut icon" href="<?php echo $theme_options['cp_favicon']; ?>" type="image/x-icon" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Comments RSS Feed" href="<?php bloginfo('comments_rss2_url') ?>"  />
<link rel="shortcut icon" href="#">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<script type="text/javascript" src="/wp-content/themes/caribbeanclub/js/cufon-yui.js"></script>
<script type="text/javascript"  src="/wp-content/themes/caribbeanclub/js/Maximus_400.font.js"></script>
<script type="text/javascript"  src="/wp-content/themes/caribbeanclub/js/Honey_Script_600.font.js"></script>
<script type="text/javascript" src="/wp-content/themes/caribbeanclub/js/stepcarousel.js"></script>
<script type="text/javascript" src="/wp-content/themes/caribbeanclub/js/imtech_pager.js"></script>
<script type="text/javascript">
var pager = new Imtech.Pager();
$(document).ready(function() {
    pager.paragraphsPerPage = 2; // set amount elements per page
    pager.pagingContainer = $('#content'); // set of main container
    pager.paragraphs = $('div.z', pager.pagingContainer); // set of required containers
    pager.showPage(1);
});
</script>
<script type="text/javascript">
   $(document).ready(function() {
      $('div.demo-show2> div').hide();  
    });

    $(document).ready(function() {
      // ...
      $('div.demo-show2> h3').click(function() {
      // ...
      });
    });

    $(document).ready(function() {
      $('div.demo-show2> div').hide();  
      $('div.demo-show2> h3').click(function() {
        $(this).next('div').slideToggle('fast')
        .siblings('div:visible').slideUp('fast');
      });
    });

</script>

<!--[if gte IE 9]>
<script type="text/javascript">  
	Cufon.replace('h3,  #contactbar', {fontFamily: 'Maximus', textShadow: '1px 1px 1px #666666 })'}); 	
	Cufon.replace('h1', {fontFamily: 'Honey Script', color: '#20a9b8', fontSize: '50em', letterSpacing: '1.2em', textShadow: '2px 2px rgba(0,0,0,.12) })'}); 
</script>
<![endif]-->





<link rel="stylesheet" href="/wp-content/themes/caribbeanclub/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/wp-content/themes/caribbeanclub/css/specials.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/wp-content/themes/caribbeanclub/step.css" type="text/css" media="screen" />

<!--[if IE]>
<link rel="stylesheet" type="text/css" href="/wp-content/themes/caribbeanclub/css/ie.css" /> 
<![endif]-->


<!--[if IE 9]>
<link rel="stylesheet" type="text/css" href="/wp-content/themes/caribbeanclub/css/ie9.css" /> 
<![endif]-->

<!--link rel="stylesheet" href="/wp-content/themes/caribbeanclub/css/chrome.css" type="text/chrome" /-->

<?php
/*
<style type="text/css">
@media screen and (-webkit-min-device-pixel-ratio:0) {

* .stepcarousel {
top:-330px;	
left:0px;
}
}

</style>
*/
?>

</head>

<body>
<div id="contactbar">
  <div id="business"> <img id="hmIcon" src="/wp-content/themes/caribbeanclub/images/hmIcon.png" border="0" />CARIBBEAN CLUB</div>
  <div id="address"> <img id="phIcon" src="/wp-content/themes/caribbeanclub/images/phIcon.png" border="0" />RESERVATIONS: 1-800-800-6981</div>
  <div id="logo"><a href="/"><img src="/wp-content/themes/caribbeanclub/images/logo.png" border="0" /></a></div>
</div>
<div id="header">
  <div id="parrot"><img src="/wp-content/themes/caribbeanclub/images/parrot.png" border="0" /></div>
  <div id="polaroid"><img src="/wp-content/themes/caribbeanclub/images/polaroids.png" border="0" /></div>
</div>
<div id="navwrap">
  <div id="navlinks"><a href="/">Home</a></div>
  <div id="navdash"></div>
  <div id="navlinks"><a href="condos">Condos</a></div>
  <div id="navdash"></div>
  <div id="navlinks"><a href="amenities">Amenities</a></div>
  <div id="navdash"></div>
  <div id="navlinks"><a href="gallery">Gallery</a></div>
  <div id="navdash"></div>
  <div id="navlinks"><a href="location">Location</a></div>
  <div id="navdash"></div>
  <div id="navlinks"><a href="packages">Packages</a></div>
  <div id="navdash"></div>
  <div id="navlinks"><a href="specials">Specials</a></div>
  <div id="navdash"></div>
  <div id="navlinks"><a href="reviews">Reviews</a></div>
  <div id="navdash"></div>
  <div id="navlinks"><a href="contact">Contact</a></div>
</div>

<div id="slidewrap">


<div id="breadcrumbs">

<div style="width:920px; height:25px; padding:5px 0px 0px 0px; text-align:right; background:transparent;">    
<div id="wpbreadcrumbs">
 <?php 
bcn_display();
 ?>
 
</div>
</div>
</div>
 
  
  
  <div id="slideDecoLeft"><img src="/wp-content/themes/caribbeanclub/images/flowerLeft.png" border="0" /></div>
  <div id="slideDecoRight"><img src="/wp-content/themes/caribbeanclub/images/flowerRight.png" border="0" /></div>
<?php echo do_shortcode('[nivo number=10 size="first" category="homepage-gallery"]'); ?> </div>
