<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'caribbea_wordpress');

/** MySQL database username */
define('DB_USER', 'caribbea_caribbe');

/** MySQL database password */
define('DB_PASSWORD', 'HtWuABecODAqmkZRxOTT');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gumPK5LBs`^v!c|u({_,p^cx-jzeN+iD5Ljc/G>~%ZP0K`x6a!zGpdZ:0R3F{L@%');
define('SECURE_AUTH_KEY',  '}+J>1glWQb?eiPQ~H0Us%t)gRpwo+M71GL%xsU+0],q]smG]YJnVwo&=tdj7 XN_');
define('LOGGED_IN_KEY',    's%z[]>CWf:?#h-uk~r,[8u6SO2W{5 D[eo<i;9z3;W]TokGhe{&GCB.:k3^/,:{B');
define('NONCE_KEY',        'ci~x!s:3|)8SLp)v*=m5KU(x8gktwE_.Qo;N0^Csm@39&r5-Xvj^V+IQv,t~)D|L');
define('AUTH_SALT',        ':NX&v)5=QvLd~SMy!V|RCl&Ur4w{Q<k,rk;y)i/r|L qZZlKiDGc<vT-,?jMmXG=');
define('SECURE_AUTH_SALT', '|3<$MZ5}>n<!~52+U+~X 3fRW:wppz,Y@)}k}oSh2MW +|l6 4+<no<FiOh86fT7');
define('LOGGED_IN_SALT',   '0w_1iAy#A{t+9bx.qPeq}we!#EC}94+Cys#!v9{I7!4z)-t|E&Do+jqE(3P01R_R');
define('NONCE_SALT',       '+iyGXpIL-zpG}U-|oX~W|._[R6`=Q<}HT8V0&w<7V7`;!SV/&.-~bQB:#Zqfjl-6');

/**#@-*/


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */

# displays all errors in the error logs and does not display php errors on the web page
ini_set('log_errors','On');
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);

/** Disable file editing */
define('DISALLOW_FILE_EDIT',true);
define('DISALLOW_FILE_MODS',true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
